FROM node:gallium-alpine

WORKDIR /usr/lib/app

COPY . .

RUN npm install
RUN npm run build
RUN npm install -g pm2

EXPOSE 3000

CMD pm2 start ./.output/server/index.mjs -i max --no-daemon

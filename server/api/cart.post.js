import Stripe from 'stripe'

export default defineEventHandler(async (event) => {
  const body = await useBody(event)
  const stripeSecret = useRuntimeConfig().stripeSecret
  const stripe = new Stripe(stripeSecret)
  const res = await stripe.products.list({
    ids: body.map(product => product.id)
  })
  const lineItems = res.data.map(product => ({
    price: product.default_price,
    quantity: body.find(p => p.id === product.id).quantity
  }))
  const session = await stripe.checkout.sessions.create({
    cancel_url: useRuntimeConfig().cdnURL + 'cart',
    success_url: useRuntimeConfig().cdnURL + 'checkout/success',
    mode: 'payment',
      line_items: lineItems
  })
  return session
})

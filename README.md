# Sauce Market

An e-commerce site specializing in hot sauces.

## Configuration

Set Contentful, Deskree and Stripe credentials in a file `.env` at the root of the project:

```
NUXT_CONTENTFUL_SPACE=
NUXT_CONTENTFUL_PUBLIC_ACCESS_TOKEN=
NUXT_DESKREE_BASE_URL=
STRIPE_SECRET=
```

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/guide/deploy/presets) for more information.

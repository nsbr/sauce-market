export const useProductStore = defineStore('ProductStore', {
  state: () => {
    const route = useRoute() 
    return {
      products: [],
      singleProduct: null,
      filters: {
        'fields.heatLevel': route.query['fields.heatLevel'] || "",
        order: route.query['order'] || "",
        query: route.query['query'] || ""
      }
    }
  },
  getters: {
    activeFilters: (state) => {
      const clone = JSON.parse(JSON.stringify(state.filters))
      // remove blank object properties
      return Object.fromEntries(
        Object.entries(clone).filter(([_, v]) => v != null)
      )
    }
  },
  actions: {
    async fetchProducts() {
      const { $contentful } = useNuxtApp()
      const res = await $contentful.getEntries({
        content_type: 'product',
        ...this.filters
      })
      this.products = res.items
      return this.products
    },
    async fetchProduct(id) {
      const { $contentful } = useNuxtApp()
      const res = await $contentful.getEntry(id, { content_type: 'product' })
      this.singleProduct = res
      return this.products
    }
  }
})


if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useProductStore, import.meta.hot))
}

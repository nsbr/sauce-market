import { watchDebounced } from '@vueuse/core'
import { Entry } from 'contentful'


export interface Product {
  name: string
  summary: string
  price: number
  description: string
  image: string
  heatLevel: string
}

export interface CartItem {
  id: string
  item: Entry<Product>
  quantity: number
  totalPrice: number
}

export const useCartStore = defineStore('CartStore', () => {
  const items = ref<CartItem[]>([])

  const itemCount = computed(() => items.value.length)
  const subtotal = computed(() => items.value.reduce((a, c) => a + c.totalPrice / 100, 0))
  const taxes = computed(() => subtotal.value * 0.1)
  const total = computed(() => subtotal.value + taxes.value)
  let resetCmd = false

  const deskree = useDeskree()
  deskree.auth.onAuthStateChange(loginLogoutCallback)

  watchDebounced(items, () => {
    deskree.user.updateCart(items.value)
  }, {
    deep: true, debounce:5000
  })

  function loginLogoutCallback(logged) {
    if (resetCmd) {
      items.value = []
      resetCmd = false
    }
    else if (logged) {
      items.value = logged.cart
    }
  }

  function reset() {
    resetCmd = true
  }

  function addItem(item: Entry<Product>) {
    const itemFound = items.value.find(elem => elem.item.sys.id === item.sys.id)
    if (itemFound) {
      itemFound.quantity++
      itemFound.totalPrice = itemFound.item.fields.price * itemFound.quantity
    } else {
      items.value.push({
        id: item.sys.id,
        item: JSON.parse(JSON.stringify(item)),
        quantity: 1,
        totalPrice: item.fields.price
      })
    }
  }
  function setItem(item: Entry<Product>, quantity: number) {
   if (quantity < 1)
     quantity = 1
   quantity = Math.floor(quantity)
   const itemFound = items.value.find(elem => elem.item.sys.id === item.sys.id)
   if (itemFound) {
     itemFound.quantity =  quantity
     itemFound.totalPrice = itemFound.item.fields.price * itemFound.quantity
   } else {
     items.value.push({
       id: item.sys.id,
       item: JSON.parse(JSON.stringify(item)),
       quantity: quantity,
       totalPrice: item.fields.price * itemFound.quantity
     })
   }
  }
  function delItem(itemId: string) {
    const itemIndex = items.value.findIndex(elem => elem.item.sys.id === itemId)
    if (itemIndex >= 0) {
      items.value.splice(itemIndex, 1)
    }
  }
  return {
    items,
    itemCount,
    subtotal,
    taxes,
    total,
    addItem,
    setItem,
    delItem,
    reset
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCartStore, import.meta.hot))
}

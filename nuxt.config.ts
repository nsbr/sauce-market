const cdnURL =  process.env.NODE_ENV === 'production' ?  'https://nsbr.dev/project/sauce_market-app/' : 'http://localhost:3000/'
const baseURL =  process.env.NODE_ENV === 'production' ?  '/project/sauce_market-app/' : '/'

export default defineNuxtConfig({
  modules: [
    '@formkit/nuxt',
    [
      '@pinia/nuxt',
      {
        autoImports: ['defineStore', 'acceptHMRUpdate']
      }
    ]
  ],
  imports: {
    dirs: ['stores'],
  },
  alias: {
    pinia: '/node_modules/@pinia/nuxt/node_modules/pinia/dist/pinia.mjs'
  },
  build: {
    transpile:
      process.env.npm_lifecycle_script === 'nuxt generate'
        ? ['contentful']
        : [],
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {}
    }
  },
  app: {
   cdnURL: cdnURL
  },
  runtimeConfig: {
    stripeSecret: process.env.STRIPE_SECRET,
    public: {
      cdnURL: cdnURL,
      contentfulSpace: process.env.NUXT_CONTENTFUL_SPACE,
      contentfulPublicAccessToken: process.env.NUXT_CONTENTFUL_PUBLIC_ACCESS_TOKEN,
      deskreeBaseUrl: process.env.NUXT_DESKREE_BASE_URL
    },
  },
  css: [
    '@/assets/css/main.css',
    '@formkit/themes/genesis'
  ]
})
